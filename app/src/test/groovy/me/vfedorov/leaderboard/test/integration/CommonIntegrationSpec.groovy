package me.vfedorov.leaderboard.test.integration

import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import me.vfedorov.leaderboard.configuration.PropertiesProvider
import me.vfedorov.leaderboard.configuration.ServerProperties
import me.vfedorov.leaderboard.server.Bootstrap
import me.vfedorov.leaderboard.server.Server
import spock.lang.Shared
import spock.lang.Specification

import java.util.concurrent.Callable
import java.util.concurrent.CyclicBarrier
import java.util.concurrent.ExecutionException
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit


class CommonIntegrationSpec extends Specification {
    // Test config
    @Shared int     port          = 9009
    @Shared String  host          = "127.0.0.1"
    @Shared boolean test          = true
    @Shared long    sessionTTL    = TimeUnit.SECONDS.toMillis(5)
    @Shared int     serverThreads = 5
    @Shared int     topScoresNum  = 15

    @Shared Server     server
    @Shared RESTClient client
    @Shared Random     rnd

    def setupSpec(){
        PropertiesProvider provider = new PropertiesProvider() {
            @Override
            ServerProperties getProps() {
                ServerProperties testProps = new ServerProperties()
                testProps.host = host
                testProps.port = port
                testProps.test = test
                testProps.sessionTTL = sessionTTL
                testProps.serverThreads = serverThreads
                testProps.topScoresNum = topScoresNum
                testProps
            }
        }
        Bootstrap.init(provider)
        server = new Server(provider, Bootstrap.getTestApi())
        server.start()

        client = new RESTClient("http://${provider.get().getHost()}:${provider.get().getPort()}/", ContentType.TEXT)
        rnd = new Random()
    }

    def auth(int userId){
        def result = client.get(path: "$userId/login", contentType: ContentType.TEXT)
        //
        return [
                code : (result as HttpResponseDecorator).getStatus(),
                data : ((result as HttpResponseDecorator).getData() as StringReader).getText()
        ]
    }
    def postScores(int levelId, String sessionKey, int score){
        return client.post(path      : "$levelId/score",
                          query      : ["sessionkey" : sessionKey ],
                          body       : score,
                          contentType: ContentType.TEXT
        )
    }
    def getScores(int levelId){
        def result =  client.get(path: "$levelId/hightscorelist")
        return [
                code : (result as HttpResponseDecorator).getStatus(),
                data : ((result as HttpResponseDecorator).getData() as StringReader).getText()
        ]
    }


    def concurrent(int count, Closure closure) {
        def values = []
        def futures = []

        ExecutorService executor = Executors.newFixedThreadPool(count)
        CyclicBarrier barrier = new CyclicBarrier(count)

        for (int i = 0; i < count; i++) {
            futures.add(executor.submit(new Callable() {
                def call() throws Exception {
                    barrier.await()
                    closure.call()
                }
            }))
        }

        for (Future future: futures) {
            try {
                def value = future.get()
                values << value
            } catch (ExecutionException e) {
                values << e.cause
            }
        }

        return values
    }
}
