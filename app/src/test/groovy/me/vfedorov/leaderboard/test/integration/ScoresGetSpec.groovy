package me.vfedorov.leaderboard.test.integration

class ScoresGetSpec extends CommonIntegrationSpec {
    def "basic scores" (){
        setup:
            def numberOfUsers = 100
        when:
            for(i in 1..numberOfUsers){
                def sessionKey = auth(i).data as String
                postScores(1,sessionKey, rnd.nextInt((1000 - 100) + 1) + 1000)
            }
        and:
            def result = getScores(1)
        then:
            result.data.split(",").size() == topScoresNum
            println(result.data)
            def last = 0
            result.data.split(",").each {
                def parts = it.split("=")
                if(last != 0){
                    def current = Integer.valueOf(parts[1])
                    assert current < last
                }
                last = Integer.valueOf(parts[1])
            }
    }
}
