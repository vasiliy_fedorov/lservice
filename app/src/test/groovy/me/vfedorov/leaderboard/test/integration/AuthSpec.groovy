package me.vfedorov.leaderboard.test.integration

import groovyx.net.http.HttpResponseException

import java.util.concurrent.TimeUnit

class AuthSpec extends CommonIntegrationSpec {
    def "basic auth" (){
        when:
            def result = auth(1)
            def code = result.code
            def data = result.data
        then:
            result != null
            !(data as String).empty
            code == 200
    }

    def "session ttl" (){
        when: "Auth and skip session ttl"
            def sessionKey = auth(1).data as String
            server.test.skipTime(sessionTTL+10, TimeUnit.MILLISECONDS)
        and: "Try to post scores with dead session"
            postScores(1,sessionKey,500)
        then: "Check that the session is failed"
            HttpResponseException exception = thrown()
            exception.message == "Forbidden"
    }

    def "session works" (){
        when: "Auth"
            def sessionKey = auth(1).data as String
        and: "Try to post scores with dead session"
            postScores(1,sessionKey,500)
        then: "Check that the session is failed"
            notThrown HttpResponseException
    }
}