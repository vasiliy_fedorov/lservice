package me.vfedorov.leaderboard.server.handlers;

import com.sun.net.httpserver.HttpExchange;
import me.vfedorov.leaderboard.model.ScoresDao;

import java.io.IOException;
import java.util.Set;

public class GetScoreRequestHandler extends HttpRequestHandler{
    private final ScoresDao scoresDao;

    public GetScoreRequestHandler(ScoresDao scoresDao) {
        this.scoresDao = scoresDao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        System.out.println("[GetScoreRequestHandler] Got get scores request: "+exchange.getRequestURI());
        String[] path = exchange.getRequestURI().getPath().split("/");
        int levelId = Integer.valueOf(path[1]);

        StringBuilder response = new StringBuilder();

        Set<ScoresDao.UserScore> resultSet = scoresDao.getHighScores(levelId,15);

        for(Object o : resultSet.toArray()){
            ScoresDao.UserScore us = (ScoresDao.UserScore)o;
            response.append(+us.userId)
                    .append("=")
                    .append(us.score)
                    .append(",");
        }

        String result = response.toString();
        okResponse(exchange,result.substring(0,result.length()-1));
    }
}
