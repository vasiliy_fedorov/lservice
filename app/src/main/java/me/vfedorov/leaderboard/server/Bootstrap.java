package me.vfedorov.leaderboard.server;

import me.vfedorov.leaderboard.configuration.PropertiesProvider;
import me.vfedorov.leaderboard.model.ScoresDao;
import me.vfedorov.leaderboard.model.UsersDao;
import me.vfedorov.leaderboard.server.handlers.GetScoreRequestHandler;
import me.vfedorov.leaderboard.server.handlers.HttpRequestHandler;
import me.vfedorov.leaderboard.server.handlers.LoginRequestHandler;
import me.vfedorov.leaderboard.server.handlers.PostScoreRequestHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Pseudo-DI.
 * Of course we could add here some resolving logic to unify resolving process (e.g. only getBean() as public from Bootstrap)
 * I made all beans based on lazy-init, singleton strategy.
 */
public class Bootstrap {
    // Sever properties
    private static PropertiesProvider propertiesProvider;
    // Server handlers
    private static LoginRequestHandler     loginHandler;
    private static PostScoreRequestHandler postHandler;
    private static GetScoreRequestHandler  getingHandler;
    private static UsersDao  usersDao;
    private static ScoresDao scoresDao;
    // Diff beans
    private static TestApi testApi;

    private static void init() { init(null);  }

    public static void init(PropertiesProvider testProvider) {
        propertiesProvider = testProvider;
        loginHandler   = new LoginRequestHandler(getBean(UsersDao.class));
        postHandler    = new PostScoreRequestHandler(getBean(UsersDao.class), getBean(ScoresDao.class));
        getingHandler  = new GetScoreRequestHandler(getBean(ScoresDao.class));
    }
    public static Map<Function<String, Boolean>, HttpRequestHandler> getHandlers(){
        HashMap<Function<String, Boolean>, HttpRequestHandler> handlers = new HashMap<>();

        handlers.put( uri -> uri.endsWith("/login")                   , loginHandler);
        handlers.put( uri -> uri.matches("/\\d/score")          , postHandler);            //todo: regexp here is a bad idea
        handlers.put( uri -> uri.matches("/\\d/hightscorelist") , getingHandler); //todo: regexp here is a bad idea

        return handlers;
    }
    private static <T> T getBean(Class<T> beanClazz){
        if(beanClazz.equals(UsersDao.class)){
            if(usersDao == null) usersDao = new UsersDao.UserInMemoryDaoImpl(getPropertiesProvider());
            return (T) usersDao;
        }
        if(beanClazz.equals(ScoresDao.class)){
            if(scoresDao == null) scoresDao = new ScoresDao.ScoresInMemoryDaoImpl(getPropertiesProvider());
            return (T) scoresDao;
        }
        return null;
    }

    private static PropertiesProvider getPropertiesProvider() {
        if(propertiesProvider == null) propertiesProvider = new PropertiesProvider();
        return propertiesProvider;
    }
    public static TestApi getTestApi(){
        if(testApi == null) testApi = new TestApi(getPropertiesProvider(), getBean(UsersDao.class));
        return testApi;
    }

    public static void main(String[] args) {
        Bootstrap.init();
        new Server(Bootstrap.getPropertiesProvider(), getTestApi()).start();
    }


}
