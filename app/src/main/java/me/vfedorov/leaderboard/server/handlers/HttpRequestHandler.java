package me.vfedorov.leaderboard.server.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import me.vfedorov.leaderboard.model.UsersDao;
import me.vfedorov.leaderboard.server.Bootstrap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public abstract class HttpRequestHandler implements HttpHandler {
    private static final int HTTP_OK_STATUS      = 200;
    private static final int HTTP_DENIED_STATUS  = 403;
    private static final int HTTP_FAILURE_STATUS = 503;

    // Handlers mapping
    protected static HttpRequestHandler getHandler(HttpExchange exchange) {
        URI uri = exchange.getRequestURI();
        for(Function<String, Boolean> checkFun : Bootstrap.getHandlers().keySet())
            if(checkFun.apply(uri.getPath()))
                return Bootstrap.getHandlers().get(checkFun);
        return null;
    }

    protected void okResponse(HttpExchange exchange, String response) throws IOException {
        response(exchange,HTTP_OK_STATUS,response);
    }
    protected void deniedResponse(HttpExchange exchange, String response) throws IOException {
        response(exchange,HTTP_DENIED_STATUS,response);
    }
    protected void failureResponse(HttpExchange exchange, String response) throws IOException  {
        response(exchange,HTTP_FAILURE_STATUS,response);
    }

    private void response(HttpExchange exchange, int status, String response) throws IOException {
        exchange.getResponseHeaders().put("Content-Type", Collections.singletonList("text/html"));
        exchange.sendResponseHeaders(status, response.getBytes().length);

        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    protected boolean checkSession(String sessionKey, UsersDao usersDao){
        return usersDao.checkSession(sessionKey);
    }

    Map<String,String> getQueryParameters(HttpExchange httpExchange) {
        String query = httpExchange.getRequestURI().getQuery();
        Map<String, String> result = new HashMap<>();
        if(query == null) return result;
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                result.put(entry[0], entry[1]);
            }
            else {
                result.put(entry[0], "");
            }
        }
        return result;
    }
    String getBody(HttpExchange exchange) throws IOException {
        InputStreamReader isr =  new InputStreamReader(exchange.getRequestBody(),"utf-8");
        BufferedReader br = new BufferedReader(isr);

        int b;
        StringBuilder buf = new StringBuilder(512);
        while ((b = br.read()) != -1) {
            buf.append((char) b);
        }

        br.close();
        isr.close();

        return buf.toString();
    }
    @Override
    abstract public void handle(HttpExchange exchange) throws IOException;

}
