package me.vfedorov.leaderboard.server;

import me.vfedorov.leaderboard.configuration.PropertiesProvider;
import me.vfedorov.leaderboard.configuration.ServerProperties;
import me.vfedorov.leaderboard.model.UsersDao;

import java.util.concurrent.TimeUnit;

public class TestApi {
    private final ServerProperties props;
    private final UsersDao         usersDao;

    TestApi(PropertiesProvider provider, UsersDao usersDao) {
        props = provider.get();
        this.usersDao = usersDao;
    }

    public void skipTime(long delta, TimeUnit unit) {
        if(!props.isTest()) throw new IllegalAccessError("Test api on server is not enabled.");

        usersDao.getAllSessions().forEach( sessionKey ->
                usersDao.setSessionTtl(sessionKey,usersDao.getSessionTtl(sessionKey) - unit.toMillis(delta))
        );
    }
}
