package me.vfedorov.leaderboard.server.handlers;

import com.sun.net.httpserver.HttpExchange;
import me.vfedorov.leaderboard.model.ScoresDao;
import me.vfedorov.leaderboard.model.User;
import me.vfedorov.leaderboard.model.UsersDao;

import java.io.IOException;
import java.util.Map;

public class PostScoreRequestHandler extends HttpRequestHandler {
    private final UsersDao usersDao;
    private final ScoresDao scoresDao;

    public PostScoreRequestHandler(UsersDao usersDao, ScoresDao scoresDao) {
        this.usersDao = usersDao;
        this.scoresDao = scoresDao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        System.out.println("[PostScoreRequestHandler] Got post scores request: "+exchange.getRequestURI());
        String[] path = exchange.getRequestURI().getPath().split("/");
        int levelId = Integer.valueOf(path[1]);
        Map<String, String> params  = getQueryParameters(exchange);

        int score   = Integer.valueOf(getBody(exchange));

        //region Service layer
        if(!params.containsKey("sessionkey")){
            deniedResponse(exchange,"No session provided!");
            return;
        }

        if(!checkSession(params.get("sessionkey"), usersDao)){
            deniedResponse(exchange,"Invalid session.");
            return;
        }
        User user = usersDao.getUserBySession(params.get("sessionkey"));

        scoresDao.setScores(levelId, user.id, score);
        //endregion

        okResponse(exchange,"");
    }
}
