package me.vfedorov.leaderboard.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import me.vfedorov.leaderboard.configuration.PropertiesProvider;
import me.vfedorov.leaderboard.configuration.ServerProperties;
import me.vfedorov.leaderboard.server.handlers.HttpRequestHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.Executors;

public class Server {

    private HttpServer httpServer;

    private final TestApi test;
    private final ServerProperties props;

    public Server(PropertiesProvider propertiesProvider, TestApi testApi){
        props = propertiesProvider.get();

        try {
            httpServer = HttpServer.create(new InetSocketAddress(props.getHost(), props.getPort()), 0);
            httpServer.createContext("/", new HttpRequestHandler() {
                @Override
                public void handle(HttpExchange exchange) throws IOException {
                    try {
                        Objects.requireNonNull(HttpRequestHandler.getHandler(exchange)).handle(exchange);
                    } catch(NullPointerException ex){
                        System.err.println("[Server] failed to process request: "+exchange.getRequestURI());
                        failureResponse(exchange, ex.getMessage());
                    }
                }
            });
            httpServer.setExecutor(Executors.newFixedThreadPool(props.getServerThreads()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        test = testApi;
    }

    public void start() {
        this.httpServer.start();
        System.out.println("[Server] Server started on "+props.getHost() + ":"+props.getPort()); // todo: logging api
    }

    public TestApi getTest() {
        return test;
    }
}
