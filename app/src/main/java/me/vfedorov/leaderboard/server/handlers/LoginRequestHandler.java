package me.vfedorov.leaderboard.server.handlers;

import com.sun.net.httpserver.HttpExchange;
import me.vfedorov.leaderboard.model.User;
import me.vfedorov.leaderboard.model.UsersDao;

import java.io.IOException;

public class LoginRequestHandler extends HttpRequestHandler {
    private final UsersDao usersDao;

    public LoginRequestHandler(UsersDao usersDao) {
        this.usersDao = usersDao;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        System.out.println("[LoginRequestHandler] Got login request: "+exchange.getRequestURI());
        String response;
        String[] path = exchange.getRequestURI().getPath().split("/");
        int userId = Integer.valueOf(path[1]);

        // region Managers Layer
        User user = usersDao.getUser(userId);
        if(user == null) user = usersDao.addUser(new User(userId));

        String currentSession = usersDao.getUserSession(user);
        if(currentSession != null && usersDao.checkSession(currentSession))
            response =  currentSession;
        else
            response = usersDao.createSession(user);
        //endregion

        okResponse(exchange,response);
    }
}
