package me.vfedorov.leaderboard.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesProvider {
    ServerProperties props;

    public PropertiesProvider(){
        props = new ServerProperties();
        try {
            Properties properties = new Properties();
            File propsFile = new File("./application.properties");
            if(propsFile.exists()) {
                properties.load(new FileInputStream("./application.properties"));
            }
            else {
                properties.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
            }

            if(properties.containsKey("server.host"))    props.setHost(properties.getProperty("server.host"));
            if(properties.containsKey("server.port"))    props.setPort(Integer.valueOf(properties.getProperty("server.port")));
            if(properties.containsKey("server.threads")) props.setServerThreads(Integer.valueOf(properties.getProperty("server.threads")));
            if(properties.containsKey("session.ttl"))    props.setSessionTTL(Long.valueOf(properties.getProperty("session.ttl")));
            if(properties.containsKey("test"))           props.setTest(properties.getProperty("test").equals("true"));
            if(properties.containsKey("scores.num"))     props.setTopScoresNum(Integer.valueOf(properties.getProperty("scores.num")));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ServerProperties get() {
        return getProps();
    }
    public ServerProperties getProps(){
        return props;
    }
}
