package me.vfedorov.leaderboard.configuration;

public class ServerProperties {
    private int     port;
    private String  host;
    private boolean test;
    private long    sessionTTL;
    private int     serverThreads;
    private int     topScoresNum;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public long getSessionTTL() {
        return sessionTTL;
    }

    public void setSessionTTL(long sessionTTL) {
        this.sessionTTL = sessionTTL;
    }

    public int getServerThreads() {
        return serverThreads;
    }

    public void setServerThreads(int serverThreads) {
        this.serverThreads = serverThreads;
    }

    public int getTopScoresNum() {
        return topScoresNum;
    }

    public void setTopScoresNum(int topScoresNum) {
        this.topScoresNum = topScoresNum;
    }
}
