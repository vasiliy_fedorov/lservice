package me.vfedorov.leaderboard.model;

import me.vfedorov.leaderboard.configuration.PropertiesProvider;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public abstract class UsersDao {
    final PropertiesProvider provider;

    UsersDao(PropertiesProvider provider) {
        this.provider = provider;
    }

    public abstract Set<User>   getAllUsers();
    public abstract User        getUser(int id);
    public abstract User        addUser(User user);

    public abstract Set<String> getAllSessions();
    public abstract boolean     checkSession(String session);
    public abstract String      createSession(User user);
    public abstract String      getUserSession(User user);
    public abstract Long        getSessionTtl(String sessionKey);
    public abstract void        setSessionTtl(String sessionKey, Long ttl);

    public abstract User getUserBySession(String sessionkey);


    public static class UserInMemoryDaoImpl extends UsersDao {
        public UserInMemoryDaoImpl(PropertiesProvider provider){
            super(provider);
        }
        Map<User, String> users          = new ConcurrentHashMap<>();
        Map<String, Long> sessions       = new ConcurrentHashMap<>();
        Map<String, User> sessionsToUser = new ConcurrentHashMap<>();

        @Override
        public Set<User> getAllUsers() { return users.keySet(); }

        @Override
        public User getUser(int id) {
            return users.keySet()
                    .stream()
                    .filter( user -> user.id == id)
                    .findAny()
                    .orElse(null);
        }

        @Override
        public User addUser(User user) {
            users.put(user, "");
            return user;
        }

        @Override
        public Set<String> getAllSessions() {
            return sessions.keySet();
        }

        @Override
        public boolean checkSession(String session) {
            Long ttl = sessions.get(session);
            if(ttl == null) return false;
            if(System.currentTimeMillis() > ttl){
                sessions.remove(session);
                return false;
            }
            return true;
        }

        @Override
        public String createSession(User user) {
            // https://en.wikipedia.org/wiki/Universally_unique_identifier#Collisions
            // I think this is “reasonably unique”. =)
            String sessionKey = UUID.randomUUID().toString();
            Long ttl = System.currentTimeMillis() + provider.get().getSessionTTL();
            sessions.put(sessionKey, ttl);
            if(getUser(user.id) == null) addUser(user);
            users.put(user,sessionKey);
            sessionsToUser.put(sessionKey, user);
            return sessionKey;
        }

        @Override
        public String getUserSession(User user) {
            String session = users.get(user);
            return session.isEmpty()?null:session;
        }

        @Override
        public Long getSessionTtl(String sessionKey) {
            return sessions.get(sessionKey);
        }

        @Override
        public void setSessionTtl(String sessionKey, Long ttl) {
            sessions.put(sessionKey,ttl);
        }

        @Override
        public User getUserBySession(String sessionkey) {
            return sessionsToUser.get(sessionkey);
        }
    }
    public static abstract class UserRedisDaoImpl extends UsersDao {
        UserRedisDaoImpl(PropertiesProvider provider) {
            super(provider);
        }
        // todo: Currently this situation is not implemented (not needed by task) but this is mock for future implementations.
    }
}
