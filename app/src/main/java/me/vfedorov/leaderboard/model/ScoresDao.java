package me.vfedorov.leaderboard.model;

import me.vfedorov.leaderboard.configuration.PropertiesProvider;

import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

public abstract class ScoresDao {
    final PropertiesProvider provider;

    ScoresDao(PropertiesProvider provider) {
        this.provider = provider;
    }

    public abstract void setScores(int levelId, int userId, int score);
    public abstract NavigableSet<UserScore> getHighScores(int levelId, int num);

    public static class UserScore implements Comparable<UserScore> {
        public int userId;
        public int score;

        UserScore(int userId, int score) {
            this.userId = userId;
            this.score = score;
        }

        @Override
        public int compareTo(UserScore o) {
            return Integer.compare(score,o.score);
        }

        public boolean equals(UserScore o) {
            return userId == o.userId;
        }
    }

    // Implementations
    public static class ScoresInMemoryDaoImpl extends ScoresDao {
        Map<Integer,NavigableSet<UserScore>> scores = new ConcurrentHashMap<>();

        public ScoresInMemoryDaoImpl(PropertiesProvider propertiesProvider) {
            super(propertiesProvider);
        }

        @Override
        public void setScores(final int levelId, final int userId, final int score) {
            if(!scores.containsKey(levelId)) scores.put(levelId,new TreeSet<>(Collections.reverseOrder()));
            ((TreeSet)scores.get(levelId)).add(new UserScore(userId,score));
        }

        @Override
        public NavigableSet<UserScore> getHighScores(final int levelId, final int num) {
            NavigableSet<UserScore> result = new TreeSet<>(Collections.reverseOrder());
            if(scores.containsKey(levelId)){
                int i = 0;
                Iterator<UserScore> iterator = scores.get(levelId).iterator();
                while(i < num && iterator.hasNext()){
                    result.add(iterator.next());
                    i++;
                }
            }
            return result;
        }
    }
}
